class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :nombre
      t.string :email
      t.string :web
      t.text :content
      t.integer :commentable_id
      t.string :commentable_type

      t.timestamps
    end
  end
end
