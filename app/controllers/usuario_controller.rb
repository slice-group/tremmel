class UsuarioController < ApplicationController
	layout 'layouts/admin'

  def index
  	@users = User.all
  end

  def new
  	@user = User.new
  end

  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to usuario_index_path, notice: 'user was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  	user = User.find(params[:id])
    user.destroy
    respond_to do |format|
      format.html { redirect_to usuario_index_path }
      format.json { head :no_content }
    end
  end
end

private

# Never trust parameters from the scary internet, only allow the white list through.
def user_params
  params.require(:user).permit(:nombre, :email, :password, :password_confirmation)
end
