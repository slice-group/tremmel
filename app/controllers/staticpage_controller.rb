class StaticpageController < ApplicationController
  def index
  	@posts = Post.order("created_at DESC")
  	@users = User.all
  end

  def contactar
  	if verify_recaptcha( :message => "Oh! Error con el reCAPTCHA!")
    	ContactMailer.contact(params).deliver
  		redirect_to root_path(anchor: 'contactanos'), notice: params[:name]+', ¡Tu mensaje ha sido en enviado!'
		else
	    redirect_to root_path(anchor: 'contactanos'), alert: params[:name]+', ¡Error!'
		end
  end
end
