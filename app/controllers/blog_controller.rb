class BlogController < ApplicationController
	layout 'layouts/application'
  def index
  	@posts = Post.order("created_at DESC")
  	@posts_recientes = Post.order("created_at DESC")
  	@users = User.all
  end
end
