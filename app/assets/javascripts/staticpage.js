function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

$( document ).ready(function() {
  $('#myCarousel2').carousel({
    interval: 5000
  });

	$('.top-nav').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 500,
    filter: ':not(.external)',
    begin: function() {
      removeHash();
    },
    end: function() {
    	
    	if ($('.top-nav .current').find('a').attr('href') == "#home") {
    		$(".navbar").animate({opacity: '0', top: '-350px'}, 500 );
      } else {
      	$(".navbar").animate({opacity: '1', top: '0px'}, 500 );

      }
    },
    scrollChange: function($currentListItem) {
     
      if($(".top-nav .current a").attr("href") != "#home"){
        $(".navbar").animate({opacity: '1', top: '0px'}, 500);
        
      } else {
      	$(".navbar").animate({opacity: '0', top: '-350px'}, 500 );

      }
      		  
    }
  });


  $('.no-active a').click(function() {    
    //Scroll to bottom
    $('html, body').animate({scrollTop: $('#home').get(0).scrollHeight}, 500, function() {  
      removeHash();
    });
    
  });


	$('.logo-header-home').animate({
		top:'-10px'
	}, 1000, function(){
		$('.divisor').css('opacity', '1');
		$('.title-home h1').animate({
			opacity: '1'
		}, 1000, function(){
			$('.v-carousel-mask').animate({
				opacity: '1'
			},1000, function(){
				$('#icon1').animate({
					 opacity: 1
				},300, function(){
					$('#icon2').animate({
						opacity: 1
					},300, function(){
						$('#icon3').animate({
							opacity: 1
						},300, function(){
							$('#icon4').animate({
								opacity: 1
							},300, function(){
								$('#icon5').animate({
									opacity: 1
								},300, function(){
									$('#icon6').animate({
										opacity: 1
									},300, function(){
										$('.icons-home p').css('opacity', '1');
									});		
								});	
							});		
						});	
					});
				});
			});
		});
	});

  $(".brand-tremmel").hover(
    function() {
      $("#trem").css("color", "#66cc00")
      $("#mel").css("color", "#238294")
      $("#brand-label").css("background-color", "rgb(173, 173, 173)")
    }, function() {
      $(".text-brand-tremmel").css("color", "white")
      $("#brand-label").css("background-color", "#494545")
    }
  );
  	
});

jQuery.extend(jQuery.validator.messages, {
  required: "Este campo es obligatorio.",
  remote: "Por favor, rellena este campo.",
  email: "Por favor, escribe una dirección de correo válida",
  url: "Por favor, escribe una URL válida.",
  date: "Por favor, escribe una fecha válida.",
  dateISO: "Por favor, escribe una fecha (ISO) válida.",
  number: "Por favor, escribe un número entero válido.",
  digits: "Por favor, escribe sólo dígitos.",
  creditcard: "Por favor, escribe un número de tarjeta válido.",
  equalTo: "Por favor, escribe el mismo valor de nuevo.",
  accept: "Por favor, escribe un valor con una extensión aceptada.",
  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
});

$(function(){ //short for $(document).ready(function(){
  $('.contact_form').validate({
    rules: {
      "email": {
        required: true,
        email: true,
        maxlength: 40
      },
      "name": {
        required: true,
        maxlength: 30
      },
      "comment": {
        required: true,
        maxlength: 1000,
        minlength: 40
      }
    }
  });
});

$(".alert").alert('close')
