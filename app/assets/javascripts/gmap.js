var map;
var markersArray = [];
var latlng = new google.maps.LatLng(11.695325, -70.208325);

function initialize()
{  
  var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };
  
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  placeMarker(latlng);
}

function placeMarker(location) {
    
	var marker = new google.maps.Marker({
    position: location, 
    map: map,
    animation: google.maps.Animation.BOUNCE,
    title:"Bella Vista, Calle Sucre #27, Punto Fijo, Falcón ",
    icon: "assets/favicon.png"
	});

	// add marker in markers array
	markersArray.push(marker);

	//map.setCenter(location);
}


google.maps.event.addDomListener(window, 'load', initialize);