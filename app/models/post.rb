class Post < ActiveRecord::Base
	belongs_to :user
	has_many :comments, as: :commentable
	mount_uploader :imagen, ImagenUploader
end
