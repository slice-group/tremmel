class ContactMailer < ActionMailer::Base
  default from: "hola@tremmelweb.com"
 
  def contact(contacto)
    @name = contacto[:name]
    @email = contacto[:email]
    @comment = contacto[:comment]
    mail(to: "hola@tremmelweb.com", subject: @name+' ha contactado con Tremmel.')
  end
end
